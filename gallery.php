<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Our Portfolio |  Manna Concepts </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Pedicure Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
        Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
                function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/gallerystyle.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    </head>
<body>
    
    <div class="baner">
        <div class="baner-overlay">
            <?php
                include ("header.php");
            ?>
        </div>
    </div>
    
    <div class="container">
        <div class="portfolio-header">
            <h2 class="h2"> our gallery </h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta sit ipsa mollitia molestias, ullam sed earum ratione adipisci architecto cupiditate perferendis eos nihil iusto fuga tenetur esse eum consequuntur alias.
            </p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="main_portfolio_content">
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/body-treats.png" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Body Treat</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/b2.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Hair Treat</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/refreshment.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Refreshment Session</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/pedicure.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Pedicure Salon</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/manicure.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Manicure Treatment</h6>
                        <a href="" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/billpayment.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Paying of Bill</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/yoghurt.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Habib Yoghurt</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
                    <img src="images/portfolio/gym.jpg" alt="" />
                    <div class="portfolio_images_overlay text-center">
                        <h6>Gym</h6>
                        <a href="#" class="btn btn-primary">Book Appointment</a>
                    </div>                              
                </div>
            </div>
        </div>
    </div>

    <div class="baner">
        <?php
            include ("footer.php");
        ?>
    </div>

    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/SmoothScroll.min.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            */
                                
            $().UItoTop({ easingType: 'easeOutQuart' });
                                
            });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script> 
</body>
</html>