<!DOCTYPE html>
<html lang="en">
	<head>
		<title> About Us | Manna Concepts </title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Pedicure Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
				<link rel="stylesheet" type="text/css" href="css/zoomslider.css" /><!--zoomslider css -->
		<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script><!--modernizer css -->
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="all" />
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	</head>
<body>
	

	<div class="baner">
		<div class="baner-overlay">
			<?php
        		include ("header.php");
    		?>
		</div>
	</div>


	<div class="about-bottom inner-padding">
		<div class="container">
			<h3 class="heading-agileinfo">About Us</span></h3>
				<div class="about-bott-right">
					<p>
						Manna Beauty Concepts & spa is a contemporary salon & spa dedicated to exceptional salon and spa services, we go the extra mile to create a memorable personal experience for each client through exceptional, pleasurable occasions tailor made for our clients. At Manna Beauty Concepts & Spa, We offer an award-winning beauty oasis with the singular goal of pampering you. Our services will restore, revive and replenish you with a unique menu of international treatments in our luxurious setting.
					</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="wthree-midd">
			<div class="container">
				<span class="contact-span">
					<i class="fa fa-map-marker" aria-hidden="true"></i>Located at Plot 142, First Avenue, Adjacent Total Filling Station, and Opposite Lugbe Unity Hospital Junction, FHA Estate, Lugbe, Abuja.
				</span>
				<p class="contact-details">
					<span class="contact-span"><i class="fa fa-phone" aria-hidden="true"></i>(+234)9020400451 </span>
					<span class="contact-span"><i class="fa fa-envelope" aria-hidden="true"></i> info@mannaconcepts.com.ng</span>
					<span class="contact-span"><i class="fa fa-clock-o" aria-hidden="true"></i> Mondays-Sundays(7:00am-10:pm)</span>
				</p>
			</div>
		</div>

		<div class="mission-div">
			<div class="container">
				<div class="row">
					<h2>how we work</h2>
					<div class="col-md-4">
						<div class="works">
							<span class="fa-stack fa-3x">
							  <i class="fa fa-circle-thin fa-stack-2x"></i>
							  <i class="fa fa-book fa-stack-1x"></i>
							</span>
							<strong>Our Mission</strong>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium explicabo quia in culpa doloremque, minus eveniet. Eaque nesciunt velit nihil odio voluptatem iure dolores similique neque totam, quisquam quaerat quae.
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="works">
							<span class="fa-stack fa-3x">
							  <i class="fa fa-circle-thin fa-stack-2x"></i>
							  <i class="fa fa-low-vision fa-stack-1x"></i>
							</span>
							<strong>Our Vission</strong>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium explicabo quia in culpa doloremque, minus eveniet. Eaque nesciunt velit nihil odio voluptatem iure dolores similique neque totam, quisquam quaerat quae.
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="works">
							<span class="fa-stack fa-3x">
							  <i class="fa fa-circle-thin fa-stack-2x"></i>
							  <i class="fa fa-users fa-stack-1x"></i>
							</span>
							<strong>Our Team</strong>
							<p>
								We work hard to create a friendly day spa environment that is inclusive and welcoming, providing affordable luxury delivered by friendly, trained professionals.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="clients">
			<div class="container">
				<div class="wthree_head_section">
					<h3 class="heading-agileinfo">Testimonials</h3>
				</div>
				<section class="slider">
					<div class="flexslider">
						<ul class="slides">
							<li>
								<img src="images/t1.jpg" alt="" />
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
									<div class="client">
										<h5>Gerald Roy</h5>
									</div>
							</li>
							<li>
								<img src="images/t2.jpg" alt="" />
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
									<div class="client">
										<h5>Adam Brandom</h5>
									</div>
							</li>
							<li>
								<img src="images/t3.jpg" alt="" />
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
									<div class="client">
										<h5>Steve Artur</h5>
									</div>
							</li>
							<li>
								<img src="images/t4.jpg" alt="" />
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
									<div class="client">
										<h5>Martin Victor</h5>
									</div>
							</li>
						</ul>
					</div>
				</section>
			</div>
		</div>

		<!-- <div class="team">
			<div class="container">
			<h3 class="heading-agileinfo">our beuticians<span>Skin,Nails and Beauty Services</span></h3>
				<div class="agile_team_grids">
					<div class="col-md-3 agile_team_grid">
						<div class="view view-sixth">
							<img src="images/t1.jpg" alt=" " class="img-responsive">
							<div class="mask">
								<h5>Lorem Ipsum</h5>
								<div class="agileits_social_icons">
									<ul class="social_agileinfo">
										<li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<h4>Daniel</h4>
						<p>Facial</p>
					</div>
					<div class="col-md-3 agile_team_grid">
						<div class="view view-sixth">
							<img src="images/t2.jpg" alt=" " class="img-responsive">
							<div class="mask">
								<h5>Lorem Ipsum</h5>
								<div class="agileits_social_icons">
									<ul class="social_agileinfo">
										<li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<h4>Mary Winkler</h4>
						<p>Regular Wax</p>
					</div>
					<div class="col-md-3 agile_team_grid">
						<div class="view view-sixth">
							<img src="images/t3.jpg" alt=" " class="img-responsive">
							<div class="mask">
								<h5>Lorem Ipsum</h5>
								<div class="agileits_social_icons">
									<ul class="social_agileinfo">
										<li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<h4>James Mac</h4>
						<p>Threading</p>
					</div>
					<div class="col-md-3 agile_team_grid">
						<div class="view view-sixth">
							<img src="images/t4.jpg" alt=" " class="img-responsive">
							<div class="mask">
								<h5>Lorem Ipsum</h5>
								<div class="agileits_social_icons">
									<ul class="social_agileinfo">
										<li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<h4>Smith Carls</h4>
						<p>Mani-Pedi</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div> -->
	</div>
	
	<div class="baner">
		<?php
        	include ("footer.php");
    	?>
	</div>
	
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		
				$(window).load(function(){
				$('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
						$('body').removeClass('loading');
					}
			});
		});
	</script> 
</body>
</html>