<!DOCTYPE html>
<html lang="en">
	<head>
		<title> Logistics |  Manna Concepts </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Pedicure Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
        Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/gallerystyle.css" rel="stylesheet" type="text/css" media="all" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
                function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	</head>
<body>
	<div class="logistics-baner">
        <div class="baner-overlay">
            <?php
                include ("header.php");
            ?>
        </div>
    </div>

    <div class="container">
        <div class="portfolio-header">
            <h2 class="h2"> Manna Logistics </h2>
            <p>
                Manna Logistics, a part of Manna Concepts, is a company that specializes in logistics using state of the art technology. In today's fast moving world, we ensure our clients flexibility, reliability, speed, accuracy, security and value. Based in Abuja, Nigeria, our logistics experts work directly with our clients to optimize their supply chains by developing and implementing customized transportation solutions that best fit their needs.
            </p>
        </div>
    </div>

	<div class="logistics-div">
		<div class="logistics-overlay">
			<div class="container">
				<div class="row">
					<h2 class="log-h2">WHAT WE DO</h2>
					<div class="col-md-4">
						<div class="service-div">
							<span><i class="fa fa-bicycle" aria-hidden="true"></i></span>
							<p class="first-p">Express Delivery Services</p>
							<p class="second-p">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur adipisci nihil enim. Voluptas tempore dignissimos, labore quibusdam iste! Eligendi delectus amet doloribus harum distinctio nihil, consequatur autem magnam nam quasi?
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service-div">
							<span><i class="fa fa-clone" aria-hidden="true"></i></span>
							<p class="first-p">PickUp & Delivery Services</p>
							<p class="second-p">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur adipisci nihil enim. Voluptas tempore dignissimos, labore quibusdam iste! Eligendi delectus amet doloribus harum distinctio nihil, consequatur autem magnam nam quasi?
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service-div">
							<span><i class="fa fa-home" aria-hidden="true"></i></span>
							<p class="first-p">Household Moving & Office Relocation</p>
							<p class="second-p">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur adipisci nihil enim. Voluptas tempore dignissimos, labore quibusdam iste! Eligendi delectus amet doloribus harum distinctio nihil, consequatur autem magnam nam quasi?
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="button-div">
				<a href="contact.php"><button>contact us today</button></a>
			</div>
		</div>
	</div>

	<div class="baner">
        <?php
            include ("footer.php");
        ?>
    </div>
</body>
</html>