
<!DOCTYPE html>
<html lang="en">
<head>
	<title> Appointment |  Manna Concepts </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Pedicure Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
			function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="css/apointment.css">
	<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
	<link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
</head>
<body>

	<div class="baner">
		<div class="baner-overlay">
			<?php
        		include ("header.php");
    		?>
		</div>
	</div>


	<div class="body">
		<div class="body-overlay">
			<div class="bg-agile">
				<div class="book-appointment">
					<h2> Make An Appointment </h2>
					<form action="#" method="post">
						<div class="left-agileits-w3layouts same">
							<div class="gaps">
								<p>Your Name</p>
								<input type="text" name="Name" placeholder="" required=""/>
							</div>	
							<div class="gaps">
								<p>Email Id</p>
								<input type="email" name="email" placeholder="" required="" />
							</div>	
							<div class="gaps">	
								<p>Contact Number</p>
								<input type="text" name="Number" placeholder="" required=""/>
							</div>
						</div>
						<div class="right-agileinfo same">
						<div class="gaps">
							<p>Book Your Date</p>		
							<input  id="datepicker1" name="Text" type="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required="">
						</div>
						<div class="gaps">
							<p>Book Your Time</p>		
							<input type="text" id="timepicker" name="Time" class="timepicker form-control" value="">	
						</div>
						<div class="gaps">
							<p>Services</p>	
							<select class="form-control">
								<option></option>
								<option>Facial Masking</option>
								<option>Nail polish</option>
								<option>Body Massage</option>
								<option>Foot Massage</option>
								<option>Special Makeup</option>
								<option>Spa Therapy</option>
								<option>Hair Treat</option>
								<option>Gym</option>
							</select>
						</div>
						<div class="gaps">
							<p>Stylist</p>	
							<select class="form-control">
								<option></option>
								<option>First Available</option>
								<option>Brian Davis</option>
								<option>Frances Nazario</option>
								<option>Natalya Stoyanova</option>
								<option>Dawn Daniels</option>
								<option>Sandi Turner</option>
								<option>Tori Voller</option>
							</select>
						</div>
						</div>
						<div class="clear"></div>
						<input type="submit" value="MAKE AN APPOINTMENT">
					</form>
				</div>
		   	</div>
		</div>
	</div>

	
	<div class="baner">
		<?php
        	include ("footer.php");
    	?>
	</div>
	
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
		<script type="text/javascript" src="js/wickedpicker.js"></script>
			<script type="text/javascript">
				$('.timepicker').wickedpicker({twentyFour: false});
			</script>
		<!-- Calendar -->
				<link rel="stylesheet" href="css/jquery-ui.css" />
				<script src="js/jquery-ui.js"></script>
				  <script>
						  $(function() {
							$( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
						  });
				  </script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script> 

</body>
</html>