<div class="footer_top_agileits">
	<div class="container">
		<div class="col-md-4 footer_grid">
			<h3>About Us</h3>
			<p>
				Manna Beauty Concepts & spa is a contemporary salon & spa dedicated to exceptional salon and spa services, we go the extra mile to create a memorable personal experience for each client.
			</p>
		</div>
		<div class="col-md-4 footer_grid">
			<h3>Navigation</h3>
			<ul class="footer_grid_list">
				<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					<a href="index.php">Homepage</a>
				</li>
				<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					<a href="about.php">About Us</a>
				</li>
				<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					<a href="gallery.php"> Our Gallery </a>
				</li>
				<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					<a href="appointment.php"> Book Appointment </a>
				</li>
				<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					<a href="services.php"> What we Do</a>
				</li>
			</ul>
		</div>
		<div class="col-md-4 footer_grid">
			<h3>Contact Info</h3>
			<ul class="address">
				<li style="text-align: center;"><i class="fa fa-map-marker" aria-hidden="true"></i>Located in Plot 142, First Avenue, Adjacent Total Filling Station, and Opposite Lugbe Unity Hospital Junction, FHA Estate, <span>Lugbe, Abuja.</span></li>
				<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="" style="color: #fff;">info@mannaconcepts.com.ng</a></li>
				<li><i class="fa fa-phone" aria-hidden="true"></i>(+234)9020400451, (+234)8170861722</li>
			</ul>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="footer_w3ls">
	<div class="container">
		<div class="footer_bottom1">
			<p> © 2018 Manna Concepts. All rights reserved | Design by <a href="">Grey Matter Agency</a></p>
		</div>
	</div>
</div>