
<!DOCTYPE html>
<html lang="en">
<head>
	<title> Services |  Manna Concepts </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Pedicure Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
			function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
</head>
<body>

	<div class="baner">
		<div class="baner-overlay">
			<?php
        		include ("header.php");
    		?>
		</div>
	</div>

	<div class="agileits-about-top">
		<div class="container">
		<h3 class="heading-agileinfo">Services<span>Skin, Hair, Nails and Beauty Services</span></h3>
			<div class="agileinfo-top-grids">
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img1"></div>
					<h4>Hair Treats</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img2"></div>
					<h4>Pedicure</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img3"></div>
					<h4>Facial</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img4"></div>
					<h4>Regular Wax</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img5"></div>
					<h4>Body Treat</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img6"></div>
					<h4>Suya Spot</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img7"></div>
					<h4>Refreshment</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<div class="service-img8"></div>
					<h4>Bill Payments</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

	<div class="wthree-subscribef-w3ls">
		<div class="container">
			<h3 class="tittlef-agileits-w3layouts white-clrf">Get All Special Offers!</h3>
			<button>
			  <a href=""></a>Contact Us Today
			</button>
		</div>
	</div>
	
	<div class="baner">
		<?php
        	include ("footer.php");
    	?>
	</div>
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script> 

</body>
</html>