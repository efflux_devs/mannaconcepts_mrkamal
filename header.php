<div class="header">
	<div class="w3-agile-logo">
		<div class="container">
			<div class=" head-wl">
				<div class="headder-w3">
					<a href="index.php"><img src="images/manna.png" alt="" style="height: 100px;width: 140px;object-fit: contain;margin-top: -12px;"></a>
				</div>
				<div class="w3-header-top-right-text">
					<h6 class="caption"> Contact Us </h6>
					<p>(+234)9020400451</p>
				</div>
				<div class="email-right">
					<h6 class="caption">Email Us</h6>
					<p><a href="" class="info"> info@mannaconcepts.com.ng</a></p>
				</div>
				<div class="agileinfo-social-grids">
					<h6 class="caption">Follow Us</h6>
					<ul>
						<li><a href="#"><span class="fa fa-facebook"></span></a></li>
						<li><a href="#"><span class="fa fa-twitter"></span></a></li>
						<li><a href="#"><span class="fa fa-instagram"></span></a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

	<div class="top-nav">
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
		   			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
	                </button>
	            </div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav ">
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="services.php">Services</a></li>
						<li><a href="logistics.php">Manna Logistics</a></li>
						<li><a href="gallery.php">Gallery</a></li>
						<li><a href="appointment.php">Book Appointment</a></li>
						<!-- <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="icons.html">Web Icons</a></li>
								<li><a href="codes.html">Short Codes</a></li>
							</ul>
						</li>  -->
						<li><a href="contact.php">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"> </div>
		</nav>	
	</div>
</div>