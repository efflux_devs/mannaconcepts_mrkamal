<!DOCTYPE html>
<html lang="en">
	<head>
		<title> Manna Concepts | Home </title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link rel="stylesheet" type="text/css" href="css/zoomslider.css" /><!--zoomslider css -->
		<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script><!--modernizer css -->
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="all" />
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	</head>
	<style>
		.about-p{
			font-size: 15px;
			line-height: 32px;
			font-family: museo;
			text-align: justify;
		}
	</style>
<body>
	<div id="demo-1" data-zs-src='["images/b2.jpg","images/b3.jpg", "images/pay.jpg", "images/laundry.jpg",  "images/b1.jpg"]' data-zs-overlay="dots">
		<div class="demo-inner-content">
		  	<div class="header">
			 	<div class="w3-agile-logo">
					<div class="container">
						<div class=" head-wl">
							<div class="headder-w3">
								<a href="index.php"><img src="images/manna.png" alt="" style="height: 100px;width: 140px;object-fit: contain;margin-top: -12px;"></a>
							</div>

							<div class="w3-header-top-right-text">
								<h6 class="caption"> Contact Us</h6>
								<p>(+234)9020400451</p>
							</div>

							<div class="email-right">
								<h6 class="caption">Email Us</h6>
								<p><a href="" class="info">info@mannaconcepts.com.ng</a></p>

							</div>


							<div class="agileinfo-social-grids">
								<h6 class="caption">Follow Us</h6>
								<ul>
									<li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-instagram"></span></a></li>
								</ul>
							</div>

							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="top-nav">
					<nav class="navbar navbar-default navbar-fixed-top">
						<div class="container">
							<div class="navbar-header">
					   			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					                <span class="sr-only">Toggle navigation</span>
					                <span class="icon-bar"></span>
					                <span class="icon-bar"></span>
					                <span class="icon-bar"></span>
				                </button>
				            </div>

							<div id="navbar" class="navbar-collapse collapse">
								<ul class="nav navbar-nav ">
									<li class="active"><a href="index.html">Home</a></li>
									<li><a href="about.php">About</a></li>
									<li><a href="services.php">Services</a></li>
									<li><a href="logistics.php">Manna Logistics</a></li>
									<li><a href="gallery.php">Gallery</a></li>
									<li><a href="appointment.php">Book Appointment</a></li>
									<!-- <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a href="icons.html">Web Icons</a></li>
											<li><a href="codes.html">Short Codes</a></li>
										</ul>
									</li>  -->
									<li><a href="contact.php">Contact</a></li>
								</ul>
							</div>
						</div>
						<div class="clearfix"> </div>
					</nav>	
				</div>
		  	</div>
			<div class="w3-banner-head-info">
				<div class="container">
				   <div class="banner-text">
						<h2>WELCOME TO MANNA BEAUTY CONCEPTS
	                     <span></span></h2>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="about-3">
		<h3 class="heading-agileinfo">Welcome<span>Skin,Hair,Nails and Beauty Services</span></h3>
		<div class="container">
			<div class="d-flex">
				<div class="about1">
					<p class="about-p">
						At Manna Beauty Concepts & spa,emphasis is placed on ensuring an alluring effect with comfort in an ambience where you experience a body exfoliation that leaves your mind and body relaxed and rejuvenated. Experience our 5 star customer service and we leave you to tell the story. Welcome onboard.
					</p>

					<p class="about-p">
						You can always expect a warm welcome when you visit our spas. Our staff are chosen not only for their technical expertise but also their positive attitudes and friendly characters. If you’re a first time visitor to a day spa we’ll be delighted to give you a tour and showcase the fantastic benefits that thermal, well-being and beauty treatments can bring you.
					</p>
					<a class="join-wthree" href="about.php"> About Us
						<i class="fa fa-hand-o-right" aria-hidden="true"></i>
					</a>
				</div>
				<div class="about2">
					
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="wthree-mid">
		<div class="container">
			<div class="row">
				<h2 class="what-we-do-h2">what we do</h2>
				<div class="col-md-3">
					<div class="service-one">
						<div class="service-id" style="left: 70px;">
							Refreshment
						</div>
					</div>
					<div class="caption">
						<p>
							Manna Beauty Concepts provides State-of-the-Art Technology to our vending clients for all of their favorite foods, snacks, and beverage items.
						</p>
						<a href="services.php">
							<a href="services.php">
								<!-- <button class="button">
									read more
								</button> -->
							</a>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service-two">
						<div class="service-id" style="left: 78px;">
							Body Treat
						</div>
					</div>
					<div class="caption">
						<ul>
							<li>Head massage</li>
							<li>Feet massage</li>
							<li>Shoulder massage</li>
							<li>Hair Treat</li>
							<li>Facial Treatment</li>
						</ul>
						<a href="services.php">
							<!-- <button class="button">
								read more
							</button> -->
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service-three">
						<div class="service-id" style="left:86px;">
							Laundry
						</div>
					</div>
					<div class="caption">
						<ul>
							<li>Free Pick-up & Home Delivery</li>
							<li>Drapery & Beddings</li>
							<li>Wedding Dresses</li>
							<li>Suit Cleaning</li>
							<li>Stain Removal</li>
						</ul>
						<a href="services.php">
							<!-- <button class="button">
								read more
							</button> -->
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service-four">
						<div class="service-id" style="left:50px;">
							General Cleaning
						</div>
					</div>
					<div class="caption">
						<p>
							At Manna Concepts we offer a variety of different cleaning services to ensure that all of your needs are met.
						</p>
						<a href="services.php">
							<!-- <button class="button">
								read more
							</button> -->
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="donails">
		<div class="donails-overlay">
			<div class="container">
				<div class="appointment-div">
					<p>
						Come leave the world behind and renew your senses with a visit to Manna Concepts Total Body Therapy.
					</p>
					<button class="donails-button"><a href="appointment.php">book an appointment</a></button>
				</div>
			</div>
		</div>
	</div>

	<div class="w3ls-section wthree-pricing">	
		<div class="container">
			<h3 class="heading-agileinfo">our membership<span></span></h3>
			<div class="pricing-grids-info">
				<div class="pricing-grid grid-one">
					<div class="w3ls-top">
						<h3>Standard</h3> 
					</div>
					<div class="w3ls-bottom">
					<h4>membership card</h4>
					<b>BENEFITS</b>
					<ul class="count">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
					
					<div class="more">
						<a href="appointment.php">Book Now</a>					
					</div>
					</div>
				</div>
				<div class="pricing-grid grid-two">
					<div class="w3ls-top">
						<h3>Silver</h3> 
					</div>
					<div class="w3ls-bottom">
					<h4>membership card</h4>
					<b>BENEFITS</b>
					<ul class="count">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
					
					<div class="more">
						<a href="appointment.php">Book Now</a>				
					</div>
					</div>
				</div>
				<div class="pricing-grid grid-three">
					<div class="w3ls-top">
						<h3>Gold</h3> 
					</div>
					<div class="w3ls-bottom">
					<h4>membership card</h4>
					<b>BENEFITS</b>
					<ul class="count">
						<li>10% discount on all Premium Package</li>
						<li>Waitlist Priority</li>
						<li>5 Complimentary Guest passes</li>
						<li>Members-only social event invites</li>
						<li></li>
					</ul>
					
					<div class="more">
						<a href="appointment.php">Book Now</a>			
					</div>
					</div>
				</div>
				<div class="zb pricing-grid grid-four">
					<div class="w3ls-top">
						<h3>Platinum</h3> 
					</div>
					<div class="w3ls-bottom">
					<h4>membership card</h4>
					<b>BENEFITS</b>
					<ul class="count">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
					
					<div class="more">
						<a href="appointment.php">Book Now</a>				
					</div>
					</div>
				</div>
				<div class="clearfix"> </div> 
			</div>
		</div>	
	</div>

	<div class="baner">
		<?php
        	include ("footer.php");
    	?>
	</div>

	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.js"></script>
 	<script type="text/javascript" src="js/jquery.zoomslider.min.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script> 

	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		
				$(window).load(function(){
				$('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
						$('body').removeClass('loading');
					}
			});
		});
	</script>

</body>
</html>